@extends('layouts.master') @section('content')
<div class="content">
	<div class="container main-container headerOffset">
		<div class="row innerPage">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="row userInfo">
					<div class="col-xs-12 col-sm-12">
						<h1 class=" text-center border-title">
							Visualyze. <span><small>By Aptimyze</small></span></span>
						</h1>
						<h4 class=" text-center border-title">Beautiful grafana like graphs
							for JMeter. Without all the hardwork.</h4>
						<hr>
						<div style="padding: 15px;">
							<p class="text-center" style="padding: 80px; font-size: 200%">Get
								beautiful Grafana based visualization and graphs for your Apache
								JMeter logs without using any plugins. Just upload and let our
								magic do the hardwork for you. See a sample dashboard <a href="#">here.</a></p>
							<a href="{{ url('/auth/register') }}" class="text-center"
								style="display: block !important;"><span
								style="padding: 10px; font-size: 225%">Signin to upload</span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
