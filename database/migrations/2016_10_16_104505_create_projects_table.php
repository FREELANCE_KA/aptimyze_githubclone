<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function(Blueprint $table) {
          $table->increments('id');
          $table->integer('user_id')->foreign('user_id')->references('id')->on('users')->nullable();
          $table->string('project_name');
          $table->string('project_type');
          $table->boolean('jmx_script_file');
          $table->boolean('datafiles');
          $table->boolean('log_file');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
